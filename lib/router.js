//Liste des routes permettant de charger les templates à afficher en fonction de la route 
FlowRouter.route('/', {
    action: function(params, queryParams) {
        BlazeLayout.render('accueil');
    }
});
FlowRouter.route('/createMission.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('createMission');
    }
});
FlowRouter.route('/inscription.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('inscription');
    }
});
FlowRouter.route('/connexion.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('connexion');
    }
});
FlowRouter.route('/nospartenaires.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('nosPartenaires');
    }
});
FlowRouter.route('/missionsList.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('missionsList');
    }
});
FlowRouter.route('/profilUtilisateurAsso.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('profilUtilisateurAsso');
    }
});
FlowRouter.route('/profilUtilisateurBene.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('profilUtilisateurBene');
    }
});
FlowRouter.route('/detailsMission.html/:missionId', {
    action: function(params, queryParams) {
        BlazeLayout.render('detailsMission');
    }
});
FlowRouter.route('/gererBenevole.html/:missionId', {
    action: function(params, queryParams) {
        BlazeLayout.render('gererBenevole');
    }
});
FlowRouter.route('/recherche.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('recherche');
    }
});
FlowRouter.route('/gererMissionBene.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('missionsListBene');
    }
});
FlowRouter.route('/mentionsLegales.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('mL');
    }
});
FlowRouter.route('/PoldeConf.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('PoldeConf');
    }
});
FlowRouter.route('/aPropos.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('aPropos');
    }
});
FlowRouter.route('/contact.html', {
    action: function(params, queryParams) {
        BlazeLayout.render('contact');
    }
});