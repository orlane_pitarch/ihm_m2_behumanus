import { Mongo } from 'meteor/mongo'

// Création de la base de données mongo qui contiendra l'ensemble des missions de notre site web
export const Missions = new Mongo.Collection('mission');

