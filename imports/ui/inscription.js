// Importation
import './inscription.html';

// Evenements du template inscription de inscription.html
Template.inscription.events({
    // Formulaire (envoyé) quand il s'agit d'un bénévole
    'submit #ForminscriptionBen': function(e){
		e.preventDefault();

        // Récupérer les valeurs des différents champs du formulaire
        var email = document.getElementById("emailBen").value;
        var password = document.getElementById("mdpBen").value;
        var prenom =  document.getElementById("prenomBen").value;
        var nom = document.getElementById("nomBen").value;
        var ddn =  document.getElementById("ddN").value;
        
		var user = {
                email: email,
                password: password,
                profile: {
                    prenom: prenom,
                    nom: nom,
                    ddn: ddn,
                    description: "",
                    telephone: "",
                    numIdentite: "",
                    interets: "",
                    profession: "",
                    situationFam: "",
                }
		}
         // Verification et insertion de l'utilisateur (champs complétés + utilisateur majeur + mdp et confirmerMdp équivalent)
        // alerte en conséquence de ce qui faux
         var ajd = new Date();
    if(email.length > 0 && prenom.length>0 && nom.length>0) {
        if (password.length > 5) {
            if(password === document.getElementById("mdpconfBen").value) {  
            // 567 648 000 000 ms = 18ans
                if((ajd - new Date(ddn)) > 567648000000) {
                    if(document.getElementById("conditionsBen").checked == true) {
                        Accounts.createUser(user, function(err) { 
                            if (err) {
                                alert(err.reason)
                            } else {
                                FlowRouter.go('/'); 
                            }
                        });
                    }
                    else {
                        alert("Vous devez accepter les conditions et la politique de confidentialité");
                    }
                }
                else {
                    alert("Il faut être majeur pour s'inscrire");
                }
            }
            else {
                alert("Les mots de passe sont différents");
            }          
        }
       else {
        alert("le mot de passe doit faire minimum 6 caractères");
       }
	} else {
        alert("Tous les champs doivent être complétés");
    }
    },

    // Formulaire quand il s'agit d'une association
    'submit #ForminscriptionAss': function(e){
		e.preventDefault();
        var nomAsso = document.getElementById("nomAss").value;
        var emailAsso = document.getElementById("emailAss").value;
        var passwordAsso = document.getElementById("mdpAss").value;
        var prenomPres =  document.getElementById("prenomPres").value;
        var nomPres = document.getElementById("nomPres").value;
        var rna =  document.getElementById("rna").value;
        
		var user = {
                email: emailAsso,
                password: passwordAsso,
                profile: {
                    nomAsso: nomAsso,
                    prenomPres: prenomPres,
                    nom: nomPres,
                    rna: rna,
                    description: "",
                    telephone: "",
                    nbActifs: "",
                    dateCreation: "",
                    nbMissionsRealisees:  "",
                    lieuSiege: ""
                }
		}
        
    if(emailAsso.length > 0 && prenomPres.length>0 && nomPres.length>0) {
        if (passwordAsso.length > 5) {
            if(passwordAsso === document.getElementById("mdpconfAss").value) {
                if(RegExp('^W+[0-9]{9}$').test(rna) == true) {
                    if(document.getElementById("conditionsAss").checked == true) {
                        Accounts.createUser(user, function(err) { 
                            if (err) {
                                alert(err.reason)
                            } else {
                                FlowRouter.go('/'); 
                            }
                        });
                    }
                    else {
                        alert("Vous devez accepter les conditions et la politique de confidentialité");
                    }
                }
                else {
                    alert("Le numero RNA n'a pas le bon format");
                }
            }
            else {
                alert("Les mots de passe sont différents");
            }          
        }
       else {
        alert("le mot de passe doit faire minimum 6 caractères");
       }
	} else {
        alert("Tous les champs doivent être complétés");
    }
    }       
});

