// Importations
import './profilUtilisateurBene.html';
import { Template } from 'meteor/templating';

// Evenements du template profilUtilisateurBene de profilUtilisateurBene.html
Template.profilUtilisateurBene.events({
    // Action lorsque l'utilisateur valide le formulaire
    'submit .publicProfileBene': function(e){
        e.preventDefault();
        // Récupération des valeurs rentrées par l'utilisateur et modification dans la base de données
        var description = document.getElementById("description").value;
        var telephone = document.getElementById("telephone").value;            
        var numIdentite = document.getElementById("numIdentite").value;
        var interets = document.getElementById("interets").value;
        var profession = document.getElementById("profession").value;
        var situationFam = document.getElementById("situationFam").value;
        Meteor.users.update({_id: Meteor.userId()}, {$set: {
            "profile.description": description,
            "profile.telephone": telephone,
            "profile.numIdentite": numIdentite,
            "profile.interets": interets,
            "profile.profession": profession,
            "profile.situationFam": situationFam,
        }});
    
    },
    // Action lorsque l'utilisateur clique sur "Supprimer mon compte"
    'click .deleteAccount': function(e) {
        if ( confirm( "Vous êtes sûr de vouloir supprimer votre compte ?" ) ) {
            Meteor.logout();
            FlowRouter.go('/');
        }
    }
});

