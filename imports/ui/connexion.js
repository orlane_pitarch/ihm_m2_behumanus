// Importation du code source HTML
import './connexion.html';

// Evenements du template connexion de connexion.html
Template.connexion.events({
    "submit form": function(event, template) {
		// Empêche que la page se réactulise
		event.preventDefault();

		// Récupération des valeurs entrées par l'utilisateur
		var user = document.getElementById("email").value;
		var password = document.getElementById("mdp").value;

		//utilisation des fonctions prédifinies dans le package Accounts de Meteor permettant la gestion des utilisateurs
		Meteor.loginWithPassword({
			email: user
		}, password, function(err) {
			// Si identifiant ou mot de passe invalide, message d'erreur
			if (err) {
				alert(err.reason)
			}
			// Sinon redirection vers la page d'accueil (en étant connecté) 
            else {
                FlowRouter.go('/'); 
            }
		});
	}
});