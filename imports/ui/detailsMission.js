// Importations
import './detailsMission.html';
import { Template } from 'meteor/templating';
import { Missions} from '../api/mission.js';

// Recuperation des détails (colonnes de la collection) de la mission à laquelle s'intéresse l'utilisateur
Template.detailsMission.helpers({
    mission(){
        // l'id de la mission est dans l'URL, on la récupére donc via les paramètre du Router
        return Missions.find({_id: FlowRouter.current().params.missionId});
    },
});

// Evenements du template detailsMission de createMission.html
Template.detailsMission.events({
    // Si l'utilisateur clique sur le bouton postuler
    "click #postuler": function(e) {
        // Si l'utilisateur n'a pas déjà postuler à cette mission, on l'ajoute comme bénévole pour la mission
        if (Missions.find({_id: FlowRouter.current().params.missionId,  benevoles : {$in : [Meteor.userId()]}}).count() != 1){
            Missions.update({ _id: FlowRouter.current().params.missionId },{ $push: { benevoles: Meteor.userId() }})
            alert("Vous venez de postuler à cette mission");
        }
        // Sinon message d'erreur
        else {
            alert("/!\ Vous avez déjà postulé à cette mission");
        }
    },
    // Si l'utilisateur clique sur le bouton contacter
    "click #contacter": function(e){
        alert("Un message a été envoyé à l'association, elle reviendra vers vous dans les plus brefs délais.");
    }
})
