//Importations
import './gererMissionBene.html';
import { Missions} from '../api/mission.js';

Template.missionsListBene.helpers({
      // Recuperation des missions auxquelles le bénévole participe (son ID est dans la liste des bénévoles de ces missions)
    mission(){
        return Missions.find({benevoles : {$in : [Meteor.userId()]}});
    },
});

// Evenements du template gererMissionBene de gererMissionBene.html
Template.missionsListBene.events({
    // Action lorsque le bénévole veut démissionner de la mission
    'click .delete': function(e){
        e.preventDefault();
        Missions.update({ _id: this._id },{ $pull: { benevoles: Meteor.userId() }})
    },
});