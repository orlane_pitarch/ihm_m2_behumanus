// Importations
import './gererBenevole.html';
import { Missions} from '../api/mission.js';

Template.gererBenevole.helpers({
    // Recuperation des détails de la mission concernée (via son ID)
    mission(){
        return Missions.find({_id: FlowRouter.current().params.missionId});
    },
    // Recuperation du nom du bénévole (le paramétre "benevole" est l'ID dans la collection users, stocké également dans les bénévoles de chaque mission)
    nom(benevole){
        if( Meteor.users.findOne({_id: benevole}) != undefined) {
            return Meteor.users.findOne({_id: benevole}).profile.nom;
        }
        
    },
    // Recuperation du prénom du bénévole
    prenom(benevole){
        if( Meteor.users.findOne({_id: benevole}) != undefined) {
            return Meteor.users.findOne({_id: benevole}).profile.prenom;
        }
    }
});

// Evenements du template gererBenevole de gererBenevole.html
Template.gererBenevole.events({
    // Losque l'association clique sur l'icone du telephone, affichage d'un message
    "click .phoneIcon": function(e){
        alert("Un message a été envoyé au bénévole, il reviendra vers vous dans les plus brefs délais.");
    }
})
