// Importation des fichiers et de la base de données
import './createMission.html';
import { Missions } from '../api/mission.js'
import './missionsList.html';
    
// Si on est dans la partie client 
if (Meteor.isClient) {
    // Evenements du template createMission de createMission.html
    Template.createMission.events({
        // Actions lorsque l'utilisateur change de valeur pour les disponibilités et la durée de la mission -> met à jour la valeur affiché pour le curseur
        'change #dispos': function (e) {
            e.preventDefault();
            newValue = document.getElementById("dispos").value;
            document.getElementById("range").innerHTML = newValue + 'h';
        },
        'change #duree': function(e){
            e.preventDefault();
            newValue = document.getElementById("duree").value;
            document.getElementById("range2").innerHTML=newValue + 'j';
        },
        // Action quand l'association annule la création d'une mission
        'click #annuleCreateMission': function(e){
            e.preventDefault();
            //Redirection à l'accueil
            FlowRouter.go('/');
        },
         // Action quand l'association valide la création d'une mission
        'submit .new-mission'(event) {

            // Empêche que la page se réactulise
            event.preventDefault();
        
            // Récupére les valeurs des différents champs du formulaire
            var titreM = document.getElementById("titre").value;
            var themeM;
            if (document.getElementById('themeChoice1').checked) {
                themeM = document.getElementById('themeChoice1').value;
            }
            else if (document.getElementById('themeChoice2').checked) {
                themeM = document.getElementById('themeChoice2').value;
            }
            else if (document.getElementById('themeChoice3').checked) {
                themeM = document.getElementById('themeChoice3').value;
            }
            else if (document.getElementById('themeChoice4').checked) {
                themeM = document.getElementById('themeChoice4').value;
            }
            var localisationM = document.getElementById("localisation").value;            
            var dispoM = document.getElementById("dispos").value;
            var dureeM = document.getElementById("duree").value;
            var nbBenevolesM = document.getElementById("nbBenevoles").value;
            var departM = document.getElementById("depart").value;
            var retourM = document.getElementById("retour").value;
            var descriptionM = document.getElementById("description").value;

            //association clef - valeur pour la base de données
            var miss = {
                proprietaire: Meteor.userId(),
                titre: titreM,
                theme: themeM,
                localisation: localisationM,
                dispo: dispoM,
                duree: dureeM,
                nombreDeBenevoles:nbBenevolesM,
                dateDeDepart: departM,
                dateDeRetour: retourM,
                descriptif: descriptionM,
                benevoles: []
            }
            
            // Verification et insertion de la mission dans la collection (tous les champs complétés + date départ < date de fin)
            if(titreM.length>0 && themeM != undefined && localisationM.length> 0 && dispoM != 0 && dureeM != 0 && departM !== '' && new Date(departM) <= new Date (retourM) && descriptionM.length>0){
                Missions.insert(miss);
                //Redirection vers la liste des missions crées par une association
                FlowRouter.go('/missionsList.html');
                //on affiche que les mission appartenant à l'association connectée 
                Missions.find({proprietaire: Meteor.userId()});
            }
            // Sinon affichage d'un message d'erreur
            else {
                alert("Tous les champs doivent être complétés !");
            } 
            
          },
    });
  }
  

