// Importation
import './missionsList.html';
import { Template } from 'meteor/templating';
import { Missions} from '../api/mission.js';

Template.missionsList.helpers({
    // Récupération des missions où le propriétaire est l'association connectée
    mission(){
        return Missions.find({proprietaire: Meteor.userId()});
    },
});

// Evenements du template missionsList de missionsList.html
Template.missionsList.events({
    // Action quand l'association clique sur le bouton x
    'click .delete': function(e){
        e.preventDefault();
        // Suppression de la mission de la base de données
        Missions.remove(this._id);
    },
});