// Importation de toutes les pages nécessaires (html s'il n'y a pas de JS nécessaire, 
// sinon on importe le JS qui se charger d'importe l'HTML lié)
import './body.html';
import './inscription.js';
import './accueil.html';
import './connexion.js';
import './createMission.js';
import './nospartenaires.js';
import './profilUtilisateurAsso.js';
import './profilUtilisateurBene.js';
import './contact.js';
import './recherche.js';
import './gererMissionBene.js';
import './mentionsLegales.html';
import './PoldeConf.html';
import './aPropos.html';
import './detailsMission.js';
import './gererBenevole.js';
import './missionsList.js';

import { Meteor } from 'meteor/meteor';

// Evenements du template body, situé dans body.html
Template.body.events({
	// Action lorsqu'on clique sur le bouton de déconnexion
    "click .logout": function() {
		// Déconnexion 
		Meteor.logout();
		// Redirection à la page d'accueil
		FlowRouter.go('/');
	},
	// Action lorsqu'on clique sur le nom du site
	"click .title": function() {
		// Redirection à la page d'accueil
		FlowRouter.go('/');
	}
});

