// Importations
import './profilUtilisateurAsso.html';
import { Template } from 'meteor/templating';

// Evenements du template profilUtilisateurAsso de profilUtilisateurAsso.html
Template.profilUtilisateurAsso.events({
    // Action lorsque l'utilisateur valide le formulaire
    'submit .publicProfileAsso': function(e){
        e.preventDefault();
        // Récupération des valeurs rentrées par l'utilisateur et modification dans la base de données
        var description = document.getElementById("description").value;
        var telephone = document.getElementById("telephone").value;            
        var nbActifs = document.getElementById("nbActifs").value;
        var creationAsso = document.getElementById("creationAsso").value;
        var nbMission = document.getElementById("nbMission").value;
        var localisation = document.getElementById("localisation").value;
        Meteor.users.update({_id: Meteor.userId()}, {$set: {
            "profile.description": description,
            "profile.telephone": telephone,
            "profile.nbActifs": nbActifs,
            "profile.creationAsso": creationAsso,
            "profile.nbMission": nbMission,
            "profile.lieuSiege": localisation,
        }});
    
    },
    // Action lorsque l'utilisateur clique sur "Supprimer mon compte"
    'click .deleteAccount': function(e) {
        if ( confirm( "Vous êtes sûr de vouloir supprimer votre compte ?" ) ) {
            Meteor.logout();
            FlowRouter.go('/');
        }
    }
});

