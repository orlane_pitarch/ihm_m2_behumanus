// Importation
import './contact.html';

// Evenements du template contact de contact.html
Template.contact.events({
    // Action lorsque l'utilisateur clique sur le bouton "Envoyer"
    "click #envoyerContact": function(e) {
        // Redirection sur la page d'accueil
        FlowRouter.go('/');
        // Message d'alerte
        alert("Votre message a été envoyé à l'équipe");
    }
})