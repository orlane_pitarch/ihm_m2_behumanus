// Importation
import './nospartenaires.html';

Template.nosPartenaires.helpers({
    partenaires() {
      // Récupére toutes les associations présentes dans la base de données
      return Meteor.users.find({ 'profile.rna' : { $exists: true } });
    },
  });
   