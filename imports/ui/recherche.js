// Importations
import './recherche.html';
import { Template } from 'meteor/templating';
import { Missions} from '../api/mission.js';
import { Session } from 'meteor/session';

// Valeurs que peut prendre "theme" au chargement de la page -> permet d'afficher toutes les missions à l'arrivée sur la page
Session.set("theme",{$in : ["Pédagogie", "Aide à la personne", "Aide en cas de crise", "Autres"]});

Template.recherche.helpers({
    // Récupère les missions qui correspondent au thème séléctionné par l'utilisateur
    mission(){
        return Missions.find({theme: Session.get("theme")});
    },
});

// Evenements du template recherche de recherche.html
Template.recherche.events({
    'change #dispos': function (e) {
        e.preventDefault();
        newValue = document.getElementById("dispos").value;
        document.getElementById("rangeDispo").innerHTML = newValue + 'h';
    },
    'change #duree': function(e){
        e.preventDefault();
        newValue = document.getElementById("duree").value;
        document.getElementById("rangeDuree").innerHTML=newValue + 'j';
    },
    'change #perimetre': function(e){
        e.preventDefault();
        newValue = document.getElementById("perimetre").value;
        document.getElementById("rangePerimetre").innerHTML=newValue + 'km';
    },
    
    'submit #formRecherche': function(e) {
        e.preventDefault();
        //on force le theme à avoir la valeur Pédagogie pour illustrer le fonctionnement de la recherche
        Session.set("theme", "Pédagogie");
    }
});